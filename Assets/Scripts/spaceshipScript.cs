﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spaceshipScript : MonoBehaviour
{
    Rigidbody2D rigidBody2D;
    public GameObject bullet;
	// Use this for initialization
	void Start ()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            rigidBody2D.velocity = new Vector3(10, 0, 0);
        else if (Input.GetKey(KeyCode.LeftArrow))
            rigidBody2D.velocity = new Vector3(-10, 0, 0);
        else
            rigidBody2D.velocity = Vector3.zero;

        // when spacebar is pressed
        if (Input.GetKeyDown(KeyCode.Space))
            Instantiate(bullet, transform.position, Quaternion.identity);
	}
}
