﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnScript : MonoBehaviour
{
    public GameObject enemy;
    public float spawnTime = 2.0f;

	// Use this for initialization
	void Start ()
    {
        // Call the 'addEnemy' function in 0 second
        // then every 'spawnTime' seconds
        InvokeRepeating("addEnemy", 0, spawnTime);
		
	}

    void addEnemy()
    {
        Renderer renderer = GetComponent<Renderer>();
        float x1 = transform.position.x - renderer.bounds.size.x / 2;
        float x2 = transform.position.x + renderer.bounds.size.x / 2;

        Vector2 spawnPoint = new Vector2(Random.Range(x1, x2), transform.position.y);

        Instantiate(enemy, spawnPoint, Quaternion.identity);

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
