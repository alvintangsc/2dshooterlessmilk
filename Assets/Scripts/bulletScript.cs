﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    public int speed;

    Rigidbody2D rigidBody2D;
    // Use this for initialization
    void Start ()
    {
        speed = 6;
        rigidBody2D = GetComponent<Rigidbody2D>();

        // Make the bullet move upward
        rigidBody2D.velocity = new Vector3(0, speed, 0);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnBecameInvisible()
    {
        Destroy(gameObject);
        Debug.Log("destroy bullet");
    }
}
