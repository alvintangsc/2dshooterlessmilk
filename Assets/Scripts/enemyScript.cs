﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyScript : MonoBehaviour
{
    public int speed = -5;
    Rigidbody2D r2d;
	// Use this for initialization
	void Start ()
    {
        r2d = GetComponent<Rigidbody2D>();

        // add a vertical speed to the enemy
        r2d.velocity = new Vector3(0, speed, 0);

        // make the enemy rotate on itself
        r2d.angularVelocity = Random.Range(-200, 200);  
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        string name = other.gameObject.name;
        if (name == "bullet(Clone)")
        {
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
        if (name == "spaceship")
        {
            Destroy(gameObject);
        }
    }
}
